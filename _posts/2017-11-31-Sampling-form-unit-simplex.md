---
title: Sampling from unit simplex
status: publish
layout: post
type: post
tags: []
categories: []
comments: true
---

The script is [available
here](https://github.com/dilawar/Scripts/blob/master/simplex_generate.py).
	
```{.python}
sage: simplex_generate.py [-h] [--dimension DIMENSION] [--number NUMBER]
[--output OUTPUT] [--method METHOD]
Generate x1,x2,...,xk such that sum(x1,x2,...,xk)=1
 
optional arguments:
-h, --help show this help message and exit
--dimension DIMENSION, -d DIMENSION
Dimention of vector (default 3)
--number NUMBER, -N NUMBER
Total number of vectors to generate (-1 for infinity).
--output OUTPUT, -o OUTPUT
Output file
--method METHOD, -m METHOD
Method (uniform sampling|TODO)
```

It writes the sampled numbers to stdout. By default, it samples from 3d space.
