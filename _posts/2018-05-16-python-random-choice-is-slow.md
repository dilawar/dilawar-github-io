---
title: Why Python's `random.choice` is bad when number of choices are small
status: publish
layout: post
type: post
tags: []
categories: []
comments: true
---

Consider two snippets. Both pick 0 or 1 with equal probabilities.

```python
a = random.choice([0,1])
```

Or,

```python
a = 1 if random.random() < 0.5 else 0
```

Later is much faster roughly 10 times faster.
<pre>In [4]: %timeit random.choice([0,1])
834 ns ± 2.19 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)

In [5]: %timeit 1 if random.random() < 0.5 else 0
101 ns ± 0.25 ns per loop (mean ± std. dev. of 7 runs, 10000000 loops each)
</pre>
How about when choices are large. Writing so many `if else` are waste of time. One can use the following approximation.

```python
a = int(random.random()*100)
```
For 100 choices, it will generate number between 0 and 100. Its performance is slightly better than <code>choice</code>.
<pre>In [15]: %timeit random.choice( range(100) )
1 µs ± 1.38 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)

In [16]: %timeit int(random.random()*100)
224 ns ± 1.23 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
</pre>
<strong>Surprise:</strong>

I thought if my choices are always in list; probably `choice` will be faster but I was wrong.
<pre>In [20]: choices = list(range(100))

In [21]: %timeit random.choice( choices )
752 ns ± 1.56 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)

</pre>
More importantly, DO NOT do this.
<pre>In [17]: choices = range(100)

In [18]: %timeit random.choice( choices )
798 ns ± 6.09 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
</pre>
